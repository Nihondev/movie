# JW Player SDK for Android

Included is the JW Player library and a demo app which showcases it.

Document version: February 26, 2021
SDK version: 3.19.2
App version: 3.19.2

## Important Links

* Full documentation for the SDK can be found here: http://developer.jwplayer.com

* Terms of Service: http://www.jwplayer.com/tos

* ExoPlayer License: https://github.com/google/ExoPlayer/blob/master/LICENSE

* Open Source demo implementation: https://github.com/jwplayer/jwplayer-sdk-android-demo

## Demo Application

The jwplayer-demo-3.19.2.apk is a simple JW Player demo.
The options menu can be used to select from a variety of streams that demo the SDK's features.
The "Enter Stream URL" menu item can be used to load your own test streams into the demo.
