﻿using Movie.Models.Component;
using Movie.Models.Dto;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Movie.Models.Dao
{
    public class MenubarListDao : BaseDao<MenubarListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<MenubarListDto> GetDataList()
        {
            try
            {
                List<MenubarListDto> list = new List<MenubarListDto>();
                dt = GetStoredProc("PD017_GET_MENUBAR");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MenubarListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        name = Util.NVLString(dr["name"]),
                        type = Util.NVLString(dr["type"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MenubarListDto> AddBar()
        {
            try
            {
                List<MenubarListDto> list = new List<MenubarListDto>();
                dt = GetStoredProc("PD014_GET_MENUBAR");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MenubarListDto
                    {
                        num = Util.NVLInt(dr["num"]),
                        name = Util.NVLString(dr["name"]),
                        type = Util.NVLString(dr["type"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MenubarListDto> GetMenuListSum()
        {
            try
            {
                List<MenubarListDto> list = new List<MenubarListDto>();
                dt = GetStoredProc("PD017_GET_MENUBAR_SUM");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MenubarListDto
                    {
                        menuList = Util.NVLString(dr["menuList"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MenubarListDto> GetMenubarYear()
        {
            try
            {
                List<MenubarListDto> list = new List<MenubarListDto>();
                dt = GetStoredProc("PD013_GET_MENUBAR_LIST_YEAR");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MenubarListDto
                    {
                        menubar_year = Util.NVLString(dr["name"]),
                        type = Util.NVLString(dr["type"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MenubarListDto> GetMenubarName()
        {
            try
            {
                List<MenubarListDto> list = new List<MenubarListDto>();
                dt = GetStoredProc("PD013_GET_MENUBAR_LIST_NAME");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MenubarListDto
                    {
                        menubar_name = Util.NVLString(dr["name"]),
                        type = Util.NVLString(dr["type"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MenubarListDto> GetMenubarType()
        {
            try
            {
                List<MenubarListDto> list = new List<MenubarListDto>();
                dt = GetStoredProc("PD013_GET_MENUBAR_LIST_TYPE");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MenubarListDto
                    {
                        menubar_type = Util.NVLString(dr["name"]),
                        type = Util.NVLString(dr["type"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public string SaveDataList(MenubarListDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD012_SAVE_MENUBAR", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@code", Util.NVLString(model.code));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@p_type", Util.NVLString(model.type));

                AddSQLParam(param, "@member_id", Util.NVLInt(1));
                AddSQLParam(param, "@active", Util.NVLInt(model.active));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public string SaveDataMenuList(MenubarListDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD012_SAVE_MENUBAR", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@code", Util.NVLString(model.code));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@p_type", Util.NVLString(model.type));

                AddSQLParam(param, "@member_id", Util.NVLInt(1));
                AddSQLParam(param, "@active", Util.NVLInt(model.active));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}