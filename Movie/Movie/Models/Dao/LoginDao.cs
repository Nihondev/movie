﻿using Movie.Models.Component;
using Movie.Models.Dto;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Movie.Models.Dao
{
    public class LoginDao : BaseDao<LoginDao>
    {
        private MySqlConnection conn;
        public Boolean isAdmin(string vUser)
        {
            Boolean flg = false;
            if (vUser.IndexOf("@ahc", 1) > 0)
            {
                flg = true;
            }
            return flg;
        }


        public bool ChkAuthDB(string vUser, ref int member_id, ref string vUName, ref int member_level, ref string password, ref string amount)
        {
            try
            {
                DataTable dt = GetStoredProc("PD01_GET_CHK_AUTH_DB", new string[] { "@USER_ID" }, new string[] { vUser });
                if (dt.Rows.Count > 0)
                {
                    member_id = Util.NVLInt(dt.Rows[0]["id"]);
                    vUName = Util.NVLString(dt.Rows[0]["username"]);
                    password = Util.NVLString(dt.Rows[0]["password"]);
                    member_level = Util.NVLInt(dt.Rows[0]["member_level"]);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public bool ChkAuthDB(string vUser, string password, ref int member_id, ref string vUName, ref string vCenter, ref string vChannel)
        {
            try
            {
                string sql = "PD020_GET_CHK_AUTH_PASS";
                DataTable dt = GetStoredProc(sql, new string[] { "@USER_ID", "@PASS" }, new object[] { vUser, password });
                if (dt.Rows.Count > 0)
                {
                    member_id = Util.NVLInt(dt.Rows[0]["id"]);
                    vUName = Util.NVLString(dt.Rows[0]["username"]);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public DataRow GetUserByID(string userID)
        {
            try
            {
                string sql = "PD013_GET_CHK_AUTH_DB";
                DataTable dt = GetStoredProc(sql, new string[] { "@username" }, new string[] { userID });
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0];
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public bool ChkUser(string vUser, ref string member_id)
        {
            try
            {
                DataTable dt = GetStoredProc("PD019_GET_CHK_AUTH_DB", new string[] { "@USER_ID" }, new string[] { vUser });
                if (dt.Rows.Count > 0)
                {
                    member_id = Util.NVLString(dt.Rows[0]["id"]);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public bool ChkUser(string vUser, string user_id)
        {
            try
            {
                DataTable dt = GetStoredProc("PD019_CHK_USER", new string[] { "@p_username", "@user_id" }, new string[] { vUser, user_id });
                return dt.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        //public void LogLogin(LogDto model, string status)
        //{
        //    try
        //    {
        //        conn = CreateConnection();
        //        MySqlCommand cmd = new MySqlCommand("PD08_SAVE_LOG_LOGIN", conn);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        MySqlParameterCollection param = cmd.Parameters;
        //        param.Clear();
        //        AddSQLParam(param, "@ip", Util.NVLString(model.ip));
        //        AddSQLParam(param, "@url", Util.NVLString(model.url));
        //        AddSQLParam(param, "@browser", Util.NVLString(model.browser));
        //        AddSQLParam(param, "@system_os", Util.NVLString(model.system_os));
        //        AddSQLParam(param, "@connect_hardware", Util.NVLString(model.connect_hardware));
        //        AddSQLParam(param, "@p_id", Util.NVLString(Varible.User.member_id));
        //        AddSQLParam(param, "@status", Util.NVLString(status));
        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        //logger.Error(ex);
        //        throw ex;
        //    }
        //}
    }
}
