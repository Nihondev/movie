﻿using Movie.Models.Component;
using Movie.Models.Dto;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Movie.Models.Dao
{
    public class MovieListDao : BaseDao<MovieListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<MovieListDto> GetDataList()
        {
            try
            {
                List<MovieListDto> list = new List<MovieListDto>();
                dt = GetStoredProc("PD006_GET_CONTENTS");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MovieListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        rating = Util.NVLString(dr["rating"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        alt = Util.NVLString(dr["alt"]),
                        url = Util.NVLString(dr["url"]),
                        time = Util.NVLString(dr["time"]),
                        teaser = Util.NVLString(dr["teaser"]),
                        sound = Util.NVLString(dr["sound"]),
                        type = Util.NVLString(dr["type"]),
                        info = Util.NVLString(dr["info"]),
                        movies = Util.NVLString(dr["movies"]),
                        img_head = Util.NVLString(dr["img_head"]),
                        year = Util.NVLString(dr["year"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        use = Util.NVLBool(dr["use"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MovieListDto> GetDataListRecom()
        {
            try
            {
                List<MovieListDto> list = new List<MovieListDto>();
                dt = GetStoredProc("PD006_GET_CONTENTS_RECOM");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MovieListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        rating = Util.NVLString(dr["rating"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        alt = Util.NVLString(dr["alt"]),
                        url = Util.NVLString(dr["url"]),
                        time = Util.NVLString(dr["time"]),
                        teaser = Util.NVLString(dr["teaser"]),
                        sound = Util.NVLString(dr["sound"]),
                        type = Util.NVLString(dr["type"]),
                        info = Util.NVLString(dr["info"]),
                        movies = Util.NVLString(dr["movies"]),
                        img_head = Util.NVLString(dr["img_head"]),
                        year = Util.NVLString(dr["year"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        use = Util.NVLBool(dr["use"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MovieListDto> GetDataSelect() //ส่ง Token เมื่อ user เลือกหนัง
        {
            try
            {
                List<MovieListDto> list = new List<MovieListDto>();
                dt = GetStoredProc("PD021_GET_MOVIE_SELECT", new string[] { "@m_id" }, new string[] { Util.NVLString(Varible.User.m_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MovieListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        rating = Util.NVLString(dr["rating"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        alt = Util.NVLString(dr["alt"]),
                        url = Util.NVLString(dr["url"]),
                        time = Util.NVLString(dr["time"]),
                        teaser = Util.NVLString(dr["teaser"]),
                        sound = Util.NVLString(dr["sound"]),
                        type = Util.NVLString(dr["type"]),
                        info = Util.NVLString(dr["info"]),
                        movies = Util.NVLString(dr["movies"]),
                        img_head = Util.NVLString(dr["img_head"]),
                        year = Util.NVLString(dr["year"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        use = Util.NVLBool(dr["use"]),

                    });
                }

               return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MovieListDto> GetDataListDash()
        {
            try
            {
                List<MovieListDto> list = new List<MovieListDto>();
                dt = GetStoredProc("PD015_GET_CONTENTS_DASHBOARD");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MovieListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        rating = Util.NVLString(dr["rating"]),
                        movies = Util.NVLString(dr["movies"]),
                        year = Util.NVLString(dr["year"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        use = Util.NVLBool(dr["use"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public string SaveDataList(MovieListDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD004_SAVE_CONTENTS", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@code", Util.NVLString(model.code));
                AddSQLParam(param, "@rating", Util.NVLString(model.rating));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@movies", Util.NVLString(model.movies));
                AddSQLParam(param, "@year", Util.NVLString(model.year));
                AddSQLParam(param, "@url", Util.NVLString(model.url));
                AddSQLParam(param, "@time", Util.NVLString(model.time));
                AddSQLParam(param, "@info", Util.NVLString(model.info));
                AddSQLParam(param, "@sound", Util.NVLString(model.sound));
                AddSQLParam(param, "@teaser", Util.NVLString(model.teaser));
                AddSQLParam(param, "@alt", Util.NVLString(model.alt));
                AddSQLParam(param, "@type", Util.NVLString(model.type));
                AddSQLParam(param, "@p_img_head", Util.NVLString(model.img_head));
                AddSQLParam(param, "@p_img_tail", Util.NVLString(model.img_tail));

                AddSQLParam(param, "@member_id", Util.NVLInt(1));
                AddSQLParam(param, "@active", Util.NVLInt(model.active));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public List<MovieListDto> GetMovieListSum()
        {
            try
            {
                List<MovieListDto> list = new List<MovieListDto>();
                dt = GetStoredProc("PD006_GET_CONTENTS_SUM");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MovieListDto
                    {
                        moviesList = Util.NVLString(dr["movies"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
}
