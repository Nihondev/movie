﻿using Movie.Models.Component;
using Movie.Models.Dto;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Movie.Models.Dao
{
    public class VisitListDao : BaseDao<VisitListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<LogDto> GetDataList(string start_date, string stop_date)
        {
            try
            {
                List<LogDto> list = new List<LogDto>();
                dt = GetStoredProc("PD04_GET_LOG_LOGIN");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new LogDto
                    {
                        num = Util.NVLInt(dr["num"]),
                        //id = Util.NVLInt(dr["id"]),
                        ip = Util.NVLString(dr["ip"]),
                        url = Util.NVLString(dr["url"]),
                        browser = Util.NVLString(dr["browser"]),
                        system_os = Util.NVLString(dr["system_os"]),
                        connect_hardware = Util.NVLString(dr["connect_hardware"]),
                        start_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        //stop_date = dr["stop_date"].ToString() == "" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["stop_date"]).ToString("yyyyMMdd"))),

                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LogDto> GetDataList()
        {
            try
            {
                List<LogDto> list = new List<LogDto>();
                dt = GetStoredProc("PD04_GET_LOG_LOGIN");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new LogDto
                    {
                        num = Util.NVLInt(dr["num"]),
                        ip = Util.NVLString(dr["ip"]),
                        url = Util.NVLString(dr["url"]),
                        browser = Util.NVLString(dr["browser"]),
                        system_os = Util.NVLString(dr["system_os"]),
                        connect_hardware = Util.NVLString(dr["connect_hardware"]),
                        start_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        //st_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMdd"))),
                        //stop_date = dr["stop_date"].ToString() == "" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["stop_date"]).ToString("yyyyMMdd"))),
                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LogDto> GetVisitSum()
        {
            try
            {
                List<LogDto> list = new List<LogDto>();
                dt = GetStoredProc("PD010_GET_VISIT_SUM");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new LogDto
                    {
                        logList = Util.NVLString(dr["amount"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }

    }
}