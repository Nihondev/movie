﻿using Movie.Models.Component;
using Movie.Models.Dto;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Movie.Models.Dao
{
    public class UserVisit : BaseDao<UserVisit>
    {
        private MySqlConnection conn;
        public void GetDataList(LogDto model, string status)
        {
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD09_SAVE_LOG_VISIT", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ip", Util.NVLString(model.ip));
                AddSQLParam(param, "@url", Util.NVLString(model.url));
                AddSQLParam(param, "@browser", Util.NVLString(model.browser));
                AddSQLParam(param, "@system_os", Util.NVLString(model.system_os));
                AddSQLParam(param, "@connect_hardware", Util.NVLString(model.connect_hardware));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
        
}