﻿using Movie.Models.Component;
using Movie.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie.Models.Dao
{
    public class GetDataDao : BaseDao<GetDataDao>
    {
        private DataTable dt;
        
        public List<LogDto> GetLogLogin(int member_id)
        {
            List<LogDto> list = new List<LogDto>();
            dt = GetStoredProc("PD039_GET_LOG", new string[] { "@memberid" }, new string[] { Util.NVLString(member_id) });
            foreach (DataRow dr in dt.Rows)
            {
                list.Add(
                    new LogDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        ip = Util.NVLString(dr["ip"]),
                        url = Util.NVLString(dr["url"]),
                        browser = Util.NVLString(dr["browser"]),
                        system_os = Util.NVLString(dr["system_os"]),
                        connect_hardware = Util.NVLString(dr["connect_hardware"]),
                        start_date = Util.NVLString(dr["start_date"]),
                        stop_date = Util.NVLString(dr["stop_date"]),
                        member_id = Util.NVLInt(dr["member_id"])
                    });

            }

            return list;
        }

        [NonAction]
        public SelectList GetYearLog()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD070_GET_YEAR_LOG");
            list.Add(new SelectListItem { Text = "년도선택", Value = "" });
            foreach (DataRow dr in dt.Rows)
            {
                list.Add(new SelectListItem { Text = Util.NVLString(dr["y"]), Value = Util.NVLString(dr["y"]) });

            }
            return new SelectList(list, "Value", "Text");
        }

        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }

    }
}