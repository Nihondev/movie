﻿using Movie.Models.Component;
using Movie.Models.Dto;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Movie.Models.Dao
{
    public class MemberListDao : BaseDao<MemberListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<MemberListDto> GetDataList()
        {
            try
            {
                List<MemberListDto> list = new List<MemberListDto>();
                dt = GetStoredProc("PD06_GET_MEMBER");
                foreach (DataRow dr in dt.Rows)
                {

                    list.Add(
                        new MemberListDto
                        {
                            id = Util.NVLInt(dr["id"]),
                            username = Util.NVLString(dr["username"]),
                            password = Util.NVLString(DataCryptography.Decrypt(dr["password"].ToString())),
                        });
                }
                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<MemberListDto> GetAdminAccountSum()
        {
            try
            {
                List<MemberListDto> list = new List<MemberListDto>();
                dt = GetStoredProc("PD011_GET_ADMIN_ACC");
                foreach (DataRow dr in dt.Rows)
                {

                    list.Add(
                        new MemberListDto
                        {
                            adminAcc = Util.NVLString(dr["adminAcc"]),
                        });
                }
                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
}