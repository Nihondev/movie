﻿using Movie.Models.Component;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie.Models.Dao
{
    public class DropdownDao : BaseDao<DropdownDao>
    {
        private DataTable dt;

        [NonAction]
        public SelectList GetDataType()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD018_GET_MENUBAR_STATUS");

            list.Add(new SelectListItem()
            {
                Text = "---ตำแหน่ง---".ToString(),
                Value = "".ToString()
            });
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["name"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetDrpSearch()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "ชื่อหนัง", Value = "movies" });
            list.Add(new SelectListItem { Text = "ปี", Value = "year" });
            return new SelectList(list, "Value", "Text");
        }
    }
}