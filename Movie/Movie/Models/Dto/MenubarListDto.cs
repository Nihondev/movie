﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie.Models.Dto
{
    public class MenubarListDto
    {
        public int id { get; set; }
        public int num { get; set; }

        [MaxLength(20)]
        public string code { get; set; }
        public string rating { get; set; }
        public string alt { get; set; }
        public string type { get; set; }
        public string p_type { get; set; }

        [MaxLength(70)]
        public string name { get; set; }
        public string status { get; set; }

        [AllowHtml]
        [MaxLength(255)]
        public string url { get; set; }
        public string movies { get; set; }
        public string menuList { get; set; }
        public string year { get; set; }
        public string moviesList { get; set; }

        [AllowHtml]

        [MaxLength(60)]
        public string include_head { get; set; }

        [MaxLength(60)]
        public string include_tail { get; set; }

        //[MaxLength(255)]
        //public string img_head { get; set; }

        [MaxLength(255)]
        public string img_tail { get; set; }
        public int active { get; set; }
        public string create_date { get; set; }
        public string menubar_year { get; set; }
        public string menubar_name { get; set; }
        public string menubar_type { get; set; }
        public string movieList_detail { get; set; }
        public bool use { get; set; }
        public string img_head { get; set; }
    }
}