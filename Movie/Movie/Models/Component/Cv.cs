﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
namespace Movie.Models.Component
{
    public class Cv
    {
        public static string Date(string date)
        {
            string _date = string.Empty;
            if (date != "")
            {
                string dd = date.Substring(6, 2);
                string MM = date.Substring(4, 2);
                int YYYY = Convert.ToInt32(date.Substring(0, 4));
                if (YYYY > 2500)
                {
                    YYYY = YYYY - 543;
                }
                if (date.Length == 8)
                {
                    _date = YYYY.ToString() + "-" + MM + "-" + dd;
                }
                else
                {
                    string TT = date.Substring(8, 8);
                    _date = YYYY.ToString() + "-" + MM + "-" + dd + " " + TT;
                }
            }
            return _date;
        }

    }
}