﻿using Movie.Models.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Movie.Models.Component
{
    public class Varible
    {
        #region for print report excel invoice

        public const string ANUAL_EXCEL = "AnualFee_{0}.xls";
        public const string IPOFEE_EXCEL = "IPOFee_{0}.xls";
        public const string OUTOFPOCKET_EXCEL = "OutOfPocket_{0}.xls";

        public static EUser _user;
        public static EConfig _config;
        #endregion

        public static EUser User
        {
            get
            {
                if (HttpRuntime.AppDomainAppId != null)
                {
                    //is web app
                    return (EUser)HttpContext.Current.Session["User"];
                }
                else
                {
                    //is windows app
                    if (_user == null)
                    {
                        _user = new EUser
                        {
                            //member_id
                            UserID = Environment.UserName,
                            UserIP = Environment.MachineName
                        };
                    }
                    return _user;
                }
            }
            set
            {
                if (HttpRuntime.AppDomainAppId != null)
                {
                    HttpContext.Current.Session["User"] = value;
                }
                else
                {
                    _user = value;
                }
            }
        }
        public static EConfig Config
        {
            get
            {
                var data = ConfigDao.Instance.GetDataList();

                _config = new EConfig
                {
                    member_icon_width = data[0].member_icon_width,
                    member_icon_height = data[0].member_icon_height,
                    member_img_width = data[0].member_img_width,
                    member_img_height = data[0].member_img_height,
                    cert_use = data[0].cert_use,
                    page_rows = data[0].page_rows,
                    jewel_price = data[0].jewel_price,
                    exchange = data[0].exchange,
                    register_point = data[0].register_point,
                    name = data[0].name,
                    moviesList = data[0].moviesList,
                };

                return _config;

            }

        }
        public enum StatusCode : int
        {
            SaveData = 1,
            RequestApprove,
            Approved,
            Rejected,
            CancelByMaker,
            CancelByApprover
        }

        public static List<string> GetddlBranch()
        {
            var res = new List<string> {
                "สำนักงานใหญ่"
                ,"สาขา"

            };
            //res.Sort();
            return res;

        }

        public const string SUCCESS = "SUCCESS";
        public const string FAIL = "FAIL";
        //TRC_CODE
        public const string MDRCW = "MDRCW";
        public const string RFEOTT = "RFEOTT";
        public const string WTMDR = "WTMDR";
        public const string MCRCW = "MCRCW";
        public const string IUOMDR = "IUOMDR";
        public const string IOMDR = "IOMDR";
        public const string IORMDR = "IORMDR";
        public const string UM_DR = "UM_DR";
        public const string UM_CR = "UM_CR";
        public const string VATDR = "VATDR";
        public const string IUOMCR = "IUOMCR";
        public const string RFEOTH = "RFEOTH";
        public const string MSMDR = "MSMDR";
        public const string HOAFCR = "HOAFCR";
        //GL_DESC
        public const string RETURN = "RETURN";
        public const string CANCEL_CONTRACT = "CANCEL_CONTRACT";

       
    }
}