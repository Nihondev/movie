﻿using Movie.Models.Component;
using Movie.Models.Dao;
using Movie.Models.Dto;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie.Controllers.Admin
{
    public class LogVisitController : Controller
    {
        Permission checkuser = new Permission();
        // GET: LogVisit
        public ActionResult Index(string start_date, string stop_date, int? page)
        {
            checkuser.chkrights("admin");
            int rows = Util.NVLInt(Varible.Config.page_rows);
            if (page != null)
            {
                if (page >= 1)
                {
                    start_date = Session["start_date"].ToString();
                    stop_date = Session["stop_date"].ToString();
                }
            }
            else
            {
                if (start_date == null)
                {
                    Session["start_date"] = Util.NVLString(Cv.Date(DateTime.Now.ToString("yyyyMMdd")));
                    Session["stop_date"] = Util.NVLString(Cv.Date(DateTime.Now.ToString("yyyyMMdd")));
                }
                else
                {
                    Session["start_date"] = start_date;
                    Session["stop_date"] = stop_date;
                }
            }
            start_date = start_date != null ? start_date + " " + "00:00:00" : Util.NVLString(Cv.Date(DateTime.Now.ToString("yyyyMMdd00:00:00")));
            stop_date = stop_date != null ? stop_date + " " + "23:59:59" : Util.NVLString(Cv.Date(DateTime.Now.ToString("yyyyMMdd23:59:59")));
            var data = VisitListDao.Instance.GetDataList(start_date, stop_date);
            TempData["start_date"] = Session["start_date"];
            TempData["stop_date"] = Session["stop_date"];
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            Session["count"] = data.Count;
            ViewBag.Count = Session["count"];
            Session["Rows"] = rows;
            return View(TempData["data"]);
        }

        public ActionResult Search(string drp, string keyword, int? page)
        {
            checkuser.chkrights("admin");
            var data = VisitListDao.Instance.GetDataList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            TempData["keyword"] = keyword;
            int cnt = data.ToList().Count;
            Session["count"] = cnt;
            ViewBag.Rows = rows;
            Session["data"] = TempData["data1"];
            ViewBag.Count = cnt;
            return View(TempData["data"]);
        }
        public ActionResult ExportToExcel()
        {
            try
            {
                var data = Session["data"] as List<LogDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "IP";
                    Sheet.Cells["B1"].Value = "접속 경로";
                    Sheet.Cells["C1"].Value = "브라우저";
                    Sheet.Cells["D1"].Value = "OS";
                    Sheet.Cells["E1"].Value = "접속기기";
                    Sheet.Cells["F1"].Value = "일시";
                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.ip.ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.url.ToString();
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.browser.ToString();
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.system_os.ToString();
                        Sheet.Cells[string.Format("E{0}", row)].Value = item.connect_hardware.ToString();
                        Sheet.Cells[string.Format("F{0}", row)].Value = item.start_date.ToString();
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();
                }
                ViewBag.Count = Session["count"];
                ViewBag.Rows = Util.NVLInt(Varible.Config.page_rows);

                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}