﻿using Movie.Models.Component;
using Movie.Models.Dao;
using Movie.Models.Dto;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Movie.Controllers.Admin
{
    public class MemberListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: MemberList
        public ActionResult Index(int? page)
        {
            if (ModelState.IsValid)
            {
                checkuser.chkrights("admin");
                var model = MemberListDao.Instance.GetDataList().Find(smodel => smodel.id == Varible.User.member_id);
                var data = MemberListDao.Instance.GetDataList();
                int rows = Util.NVLInt(Varible.Config.page_rows);
                var count = data.Count;
                TempData["data1"] = data.ToList();
                return View(model);
            }
            else
            {
                return View();
            }

        }
        public ActionResult ChkUserID(string pUid, string user_id)
        {
            try
            {
                bool result = LoginDao.Instance.ChkUser(pUid, user_id);
                if (result == false)// false = ไม่มี username นี้
                {
                    return Json(new returnsave { err = "0", errmsg = "사용 가능한 아이디 입니다.\n회원가입을 진행해 주세요." });
                }
                else
                {
                    //Session["member_id"] = member_id;
                    return Json(new returnsave { err = "1", errmsg = "사용중인 아이디 입니다. 다른 아이디를 입력해 주세요." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }


            //return objreturnsave;
        }
        public ActionResult Create()
        {
            checkuser.chkrights("admin");
            return View();
        }
        int member_id = 0;
        // POST: MemberList/Create

    }
}