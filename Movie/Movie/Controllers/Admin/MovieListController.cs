﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Movie.Models.Component;
using Movie.Models.Dao;
using Movie.Models.Dto;
using MySqlConnector;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie.Controllers.Admin
{
    public class MovieListController : Controller
    {

        Permission checkuser = new Permission();
        // GET: MovieList
        public ActionResult Index(string start_date, string stop_date, string keyword, string drp,  int? page)
        {
            checkuser.chkrights("admin");
            var data = MovieListDao.Instance.GetDataList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            if (keyword != null)
            {
                TempData["data"] = data.Where(x => x.movies.Contains(keyword) || keyword == null).ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);
        }
    
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(MovieListDto model)
        {
            try
            {
                string result = MovieListDao.Instance.SaveDataList(model, "add");
                if (result != "OK")
                {
                    ViewBag.Message = result;
                    ModelState.Clear();
                }
                else
                {

                    ViewBag.Message = "Successfully !!";
                    //KRPBL.Component.Common.Form.SetAlertMessage(this,"ไม่พบ User ID ที่ระบุ กรุณาตรวจสอบ");
                    ModelState.Clear();
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        public ActionResult Edit(int id)
        {
            var model = MovieListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            return View(model);
        }
        // POST: ContentList/Edit/5
        [HttpPost]
        public ActionResult Edit(MovieListDto model)
        {
            try
            {

                string result = MovieListDao.Instance.SaveDataList(model, "edit");
                if (result != "OK")
                {
                    ViewBag.Message = result;
                }
                else
                {
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Delete(MovieListDto model)
        {
            try
            {
                string result = MovieListDao.Instance.SaveDataList(model, "del");
                if (result == "OK")
                {
                    ViewBag.Message = "Student Deleted Successfully";
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message.ToString();
                return View();
            }
        }
        public ActionResult UploadFiles(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = FileUploadDao.Instance.Upload(file);



                    ViewBag.FileStatus = "File uploaded successfully.";
                }
                catch (Exception ex)
                {
                    ViewBag.FileStatus = "Error while file uploading."; ;
                }
            }
            return View("Index");
        }
    }

}