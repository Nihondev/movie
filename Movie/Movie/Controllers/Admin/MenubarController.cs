﻿using Movie.Models.Component;
using Movie.Models.Dao;
using Movie.Models.Dto;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Office2010.Excel;
using MySqlConnector;
using System.Data;

namespace Movie.Controllers.Admin
{
    public class MenubarController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Menubar
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var data = MenubarListDao.Instance.GetDataList();
            string count = string.Empty;
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }

            int rows = Util.NVLInt(Varible.Config.page_rows);
            ViewBag.Count = count;
            ViewBag.Rows = rows;
            return View(data.ToList().ToPagedList(page ?? 1, rows));
        }
        public ActionResult AddBar()
        {
            ViewBag.menubar_type = DropdownDao.Instance.GetDataType();
            return View();
        }
        [HttpPost]
        public ActionResult AddBar(MenubarListDto model)
        {
            try
            {
                string result = MenubarListDao.Instance.SaveDataList(model, "add");
                if (result != "OK")
                {
                    ViewBag.Message = result;
                    ModelState.Clear();
                }
                else
                {
                    ViewBag.menubar_type = DropdownDao.Instance.GetDataType();
                    ViewBag.Message = "Successfully !!";
                    //KRPBL.Component.Common.Form.SetAlertMessage(this,"ไม่พบ User ID ที่ระบุ กรุณาตรวจสอบ");
                    ModelState.Clear();
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        public ActionResult Edit(int id)
        {
            var model = MenubarListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            ViewBag.menubar_type = DropdownDao.Instance.GetDataType();
            return View(model);
        }
        // POST: ContentList/Edit/5
        [HttpPost]
        public ActionResult Edit(MenubarListDto model)
        {
            try
            {

                string result = MenubarListDao.Instance.SaveDataList(model, "edit");
                if (result != "OK")
                {
                    ViewBag.Message = result;
                }
                else
                {
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
        public ActionResult DeleteBar(MenubarListDto model)
        {
            try
            {
                string result = MenubarListDao.Instance.SaveDataMenuList(model, "del");
                if (result == "OK")
                {
                    ViewBag.Message = "Student Deleted Successfully";
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message.ToString();
                return View();
            }
        }
    }
}