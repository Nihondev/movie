﻿using Movie.Models.Component;
using Movie.Models.Dao;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie.Controllers.Admin
{
    public class DashboardController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Dashboard
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            //var data = MovieListDao.Instance.GetMovieListSum();
            ViewBag.moviesList = MovieListDao.Instance.GetMovieListSum();
            ViewBag.movies = MovieListDao.Instance.GetDataListDash();
            ViewBag.Addbar = MenubarListDao.Instance.AddBar();
            ViewBag.menuList = MenubarListDao.Instance.GetMenuListSum();
            ViewBag.logList = VisitListDao.Instance.GetVisitSum();
            ViewBag.adminAcc = MemberListDao.Instance.GetAdminAccountSum();
            //ViewBag.moviesList = Varible.Config.moviesList;
            return View();
        }

    }
}