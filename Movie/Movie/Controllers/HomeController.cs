﻿using Movie.Models.Component;
using Movie.Models.Dao;
using Movie.Models.Dto;
using MySqlConnector;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie.Controllers
{
    public class HomeController : Controller
    {
        Permission checkuser = new Permission();
        public ActionResult Index(int? page, string keyword, string drp)
        {
            //checkuser.chkrights("admin");
            EUser user = new EUser();
            user.UserIP = GetUserIP();
            Varible.User = user;
            var da = Request.Browser.IsMobileDevice;
            LogDto log = new LogDto();
            log.ip = user.UserIP;
            log.url = Request.Url.AbsoluteUri;
            log.browser = GetWebBrowserName();
            log.system_os = Info.GetHardware();
            log.connect_hardware = Request.Browser.IsMobileDevice == true ? "Mobile" : "Computer";
            UserVisit.Instance.GetDataList(log, "Visit");

            ViewBag.menubar_year = MenubarListDao.Instance.GetMenubarYear();
            ViewBag.menubar_name = MenubarListDao.Instance.GetMenubarName();
            ViewBag.menubar_type = MenubarListDao.Instance.GetMenubarType();

            var data = MovieListDao.Instance.GetDataList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            if (keyword != null)
            {
                TempData["data"] = data.Where(x => x.movies.Contains(keyword) || keyword == null ).ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);
        }

        public ActionResult Watch(string Token, int? page)  //string Token 
        {
            //checkuser.chkrights("admin");
            try
            {
                int id = Util.NVLInt(DataCryptography.Decrypt(Token));
                ViewBag.menubar_year = MenubarListDao.Instance.GetMenubarYear();
                ViewBag.menubar_name = MenubarListDao.Instance.GetMenubarName();
                ViewBag.menubar_type = MenubarListDao.Instance.GetMenubarType();
                ViewBag.movieList_detail = MovieListDao.Instance.GetDataList();
                ViewBag.movieList_recom = MovieListDao.Instance.GetDataListRecom();
                var data = MovieListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
                return View(data);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private string GetUserIP()
        {
            string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return Request.ServerVariables["REMOTE_ADDR"];
        }
        public string GetWebBrowserName()
        {
            string WebBrowserName = string.Empty;
            try
            {
                WebBrowserName = Request.Browser.Browser;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return WebBrowserName;
        }
        
    }
}